package fr.renan.jeu;

/*
 * La classe Chrono g�re un flux ind�pendant du flux principal qui execute la methode run()
 */

public class Chrono implements Runnable{
	
	private final int PAUSE = 1; //Temps d'attente entre 2 tours de boucle
	
	@Override
	public void run() {
		
		while(true) {
			MainProgram.scene.repaint();
			
			try {
			Thread.sleep(PAUSE);
			} catch (InterruptedException e) {}
		}
		
	}

}
