package fr.renan.jeu;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import fr.renan.audio.Audio;

/*
 * La classe Clavier g�re les actions utilisateurs au clavier
 */

public class Clavier implements KeyListener{

	
	//METHODS
	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_RIGHT) {
			if(MainProgram.scene.getxPos() == -1) {
				MainProgram.scene.setxPos(0);
				MainProgram.scene.setxFond1(-50);
				MainProgram.scene.setxFond2(750); }
			
			MainProgram.scene.eRobot.setMarche(true);
			MainProgram.scene.eRobot.setVersDroite(true);
			MainProgram.scene.setDx(1);
			
		} else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
			if(MainProgram.scene.getxPos() == 4000) {
				MainProgram.scene.setxPos(3999);
				MainProgram.scene.setxFond1(-50);
				MainProgram.scene.setxFond2(750); }
			
			MainProgram.scene.eRobot.setMarche(true);
			MainProgram.scene.eRobot.setVersDroite(false);
			MainProgram.scene.setDx(-1);
		}	
		//Mario saute
		if(e.getKeyCode() == KeyEvent.VK_SPACE) {
			MainProgram.scene.eRobot.setSaut(true);
			Audio.playSound("/audio/Saut.wav");
		}
	}


	@Override
	public void keyReleased(KeyEvent e) {
		MainProgram.scene.eRobot.setMarche(false);
		MainProgram.scene.setDx(0);	
	}	


    @Override
    public void keyTyped(KeyEvent e) {}

    
}