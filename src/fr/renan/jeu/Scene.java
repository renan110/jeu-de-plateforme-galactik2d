package fr.renan.jeu;


import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JPanel;


import fr.renan.affichage.Score;
import fr.renan.affichage.Temps;
import fr.renan.audio.Audio;
import fr.renan.objet.Billet;
import fr.renan.objet.Bloc;
import fr.renan.objet.Mur;
import fr.renan.objet.Objet;
import fr.renan.personnages.ExtraRobot;


/*
 * La classe scene est la plus importante de l'application
 * Accessible par toutes les autres classes, contient le "moteur" de l'application
 * Elle g�re la partie graphique !
 */


@SuppressWarnings("serial")
public class Scene extends JPanel{
	
	//FIELDS
	private ImageIcon icoFond;
	private Image imgFond1;
	private Image imgFond2;
	
	
	private ImageIcon icoOvni;
	private Image imgOvni;
	
	private ImageIcon icoFire;
	private Image imgFire;
	
	private int xFond1; //Coin sup�rieur gauche de l'image fond
	private int xFond2;
	private int dx; //Variable qui permet de deplacer ecran horizontalement
	private int xPos; //position absolu dans le jeu
	private int ySol; //hauteur courante du sol
	private int hauteurPlafond; //hauteur courante du plafond
	
	
	//INSTANCE DES PERSO / OBJETS...
	public ExtraRobot eRobot;
	
	
	public Mur mur1;
	public Mur mur2;
	public Mur mur3;
	public Mur mur4;
	public Mur mur5;
	public Mur mur6;
	public Mur mur7;
	public Mur mur8;
	
	public Bloc bloc1;
	public Bloc bloc2;
	public Bloc bloc3;
	public Bloc bloc4;
	public Bloc bloc5;
	public Bloc bloc6;
	public Bloc bloc7;
	public Bloc bloc8;
	public Bloc bloc9;
	public Bloc bloc10;
	public Bloc bloc11;
	public Bloc bloc12;
	
	public Billet billet1;
	public Billet billet2;
	public Billet billet3;
	public Billet billet4;
	public Billet billet5;
	public Billet billet6;
	public Billet billet7;
	public Billet billet8;
	public Billet billet9;
	public Billet billet10;
	
	
	private ImageIcon icoGuichet;
	private Image imgGuichet;
	private ImageIcon icoPortail;
	private Image imgPortail;
	
 
	private ArrayList<Objet> tabObjets;
	private ArrayList<Billet> tabBillets;
	
	private Score score;
	private Font police;
	
	private Temps temps;
	

	
	//CONSTRUCTOR
	public Scene() {
		
		super();
		
		this.xFond1 = -50;
		this.xFond2 = 750;
		this.dx = 0;
		this.xPos = -1;
		this.ySol = 320;
		this.hauteurPlafond = 0;
		
		icoFond = new ImageIcon(getClass().getResource("/images/Fond1.png"));
		this.imgFond1 = this.icoFond.getImage();
		this.imgFond2 = this.icoFond.getImage();
		
		icoOvni = new ImageIcon(getClass().getResource("/images/OVNI.png"));
		this.imgOvni = this.icoOvni.getImage();
		
		icoFire = new ImageIcon(getClass().getResource("/images/Fire.png"));
		this.imgFire = this.icoFire.getImage();
		
		
		// INSTANCES PERSO / OBJETS
		eRobot = new ExtraRobot(300, 270); //Instance de la clase ExtraRobot 
		
		
		mur1 = new Mur(600, 230);
		mur2 = new Mur(1100, 230);
		mur3 = new Mur(1600, 230);
		mur4 = new Mur(2100, 230);
		mur5 = new Mur(2600, 230);
		mur6 = new Mur(3100, 230);
		mur7 = new Mur(3600, 230);
		mur8 = new Mur(4100, 230);
		
		bloc1 = new Bloc(450, 200);
		bloc2 = new Bloc(750, 200);
		bloc3 = new Bloc(1250, 200);
		bloc4 = new Bloc(1300, 150);
		bloc5 = new Bloc(1350, 100);
		bloc6 = new Bloc(1900, 200);
		bloc7 = new Bloc(2300, 200);
		bloc8 = new Bloc(2400, 200);
		bloc9 = new Bloc(2700, 200);
		bloc10 = new Bloc(2750, 200);
		bloc11 = new Bloc(3400, 200);
		bloc12 = new Bloc(3800, 200);
		
		billet1 = new Billet(450 , 170);
		billet2 = new Billet(1100 ,150);
		billet3 = new Billet(1250 ,170);
		billet4 = new Billet(1300 ,125);
		billet5 = new Billet(1350 ,75);
		billet6 = new Billet(2200 ,150);
		billet7 = new Billet(2650 ,130);
		billet8 = new Billet(3000 ,170);
		billet9 = new Billet(3450 ,240);
		billet10 = new Billet(4000 ,250);
		
		
		
		icoPortail = new ImageIcon(getClass().getResource("/images/Portail.png"));
		this.imgPortail = this.icoPortail.getImage();
		
		icoGuichet = new ImageIcon(getClass().getResource("/images/Guichet.png"));
		this.imgGuichet = this.icoGuichet.getImage();
		
		tabObjets = new ArrayList<Objet>();
		
		this.tabObjets.add(this.mur1);
		this.tabObjets.add(this.mur2);
		this.tabObjets.add(this.mur3);
		this.tabObjets.add(this.mur4);
		this.tabObjets.add(this.mur5);
		this.tabObjets.add(this.mur6);
		this.tabObjets.add(this.mur7);
		this.tabObjets.add(this.mur8);
		
		this.tabObjets.add(this.bloc1);
		this.tabObjets.add(this.bloc2);
		this.tabObjets.add(this.bloc3);
		this.tabObjets.add(this.bloc4);
		this.tabObjets.add(this.bloc5);
		this.tabObjets.add(this.bloc6);
		this.tabObjets.add(this.bloc7);
		this.tabObjets.add(this.bloc8);
		this.tabObjets.add(this.bloc9);
		this.tabObjets.add(this.bloc10);
		this.tabObjets.add(this.bloc11);
		this.tabObjets.add(this.bloc12);
		
		tabBillets = new ArrayList<Billet>();
		this.tabBillets.add(this.billet1);
		this.tabBillets.add(this.billet2);
		this.tabBillets.add(this.billet3);
		this.tabBillets.add(this.billet4);
		this.tabBillets.add(this.billet5);
		this.tabBillets.add(this.billet6);
		this.tabBillets.add(this.billet7);
		this.tabBillets.add(this.billet8);
		this.tabBillets.add(this.billet9);
		this.tabBillets.add(this.billet10);
		
		
	
		
		this.setFocusable(true);
		this.requestFocusInWindow();
		this.addKeyListener(new Clavier());
		
		score = new Score();
		
		
		police = new Font("Arial", Font.BOLD, 16);
		
		temps = new Temps();
		
		
		
		Thread chronoEcran = new Thread(new Chrono());
		chronoEcran.start();
		
		
	}
	
	//GETTERS 
	public int getxPos() {return xPos; }
	
	public int getDx() {return dx; }
	
	public int getySol() {return ySol; }

	public int getHauteurPlafond() {return hauteurPlafond; }

	//SETTERS
	public void setxPos(int xPos) {this.xPos = xPos; }

    public void setDx(int dx) {this.dx = dx; }
    
	public void setxFond1(int xFond1) {this.xFond1 = xFond1; }

	public void setxFond2(int xFond2) {this.xFond2 = xFond2;}
	
	public void setHauteurPlafond(int hauteurPlafond) {this.hauteurPlafond = hauteurPlafond; }
	
	public void setySol(int ySol) {this.ySol = ySol; }

	//METHODS
	public void deplacementFond() {
		
		if(this.xPos >= 0 && this.xPos < 5000) {
			this.xPos = this.xPos + this.dx;
			this.xFond1 = this.xFond1 - this.dx;
			this.xFond2 = this.xFond2 - this.dx;
		}
		
		
		//Fond continue vers la droite
		if(xFond1 == -800) {
			xFond1 = 800;
		} else if (xFond2 == -800) {
			xFond2 = 800;
			
	    //Fond continue vers la gauche
		} else if (xFond1 == 800) {
			xFond1 = -800;
	    } else if (xFond2 == 800) {
			xFond2 = -800;
	    }
	}
	
	private boolean win() {
		if(this.score.getNbreBillets() == 10 && this.temps.getCompteurTemps() > 0 && this.xPos > 4300) {
			return true;
		} else {
			return false;
		}
	}
	
	private boolean lose() {
		if(this.temps.getCompteurTemps() == 0) {
			Audio.playSound("/audio/cringe.wav");
			return true;
		} else {
			return false;
		}
	}
	
	private boolean finPartie() {
		if (win() == true || lose() == true) {
			return true;
		} else { 
			return false;
		}
	}
	
	
	

	public void paintComponent(Graphics g) { //Dessin de toutes les images visibles � l'ecran
		
		
		super.paintComponent(g);
		Graphics g2 = (Graphics2D)g; 
		
		
		//Detection contact avec un objet
		for (int i = 0; i < this.tabObjets.size(); i++) {
			if(this.eRobot.proche(tabObjets.get(i))) { this.eRobot.contactObjet(tabObjets.get(i)); }
		}
		
		//Detection contact avec billets
		for (int i = 0; i < this.tabBillets.size(); i++) {
			if(this.eRobot.proche(this.tabBillets.get(i))) { 
				if (this.eRobot.contactBillet(this.tabBillets.get(i))){
				Audio.playSound("/audio/Billet.wav");
				this.tabBillets.remove(i);
				this.score.setNbreBillets(this.score.getNbreBillets() + 1);
				} 
			}
		}
			
		//Deplacement objets fixes du jeu
		this.deplacementFond();
		if(this.xPos >= 0 && this.xPos < 5000) {
		for (int i = 0; i < this.tabObjets.size(); i++) {this.tabObjets.get(i).deplacement(); } 
		for (int i = 0; i < this.tabBillets.size(); i++) {this.tabBillets.get(i).deplacement(); }
		
		}
		
		
		//Images :
		g2.drawImage(this.imgFond1, this.xFond1, 0, null); 
		g2.drawImage(this.imgFond2, this.xFond2, 0, null);
		g2.drawImage(this.imgOvni, 10 - this.xPos, 230, null);
		g2.drawImage(this.imgFire, -110 - this.xPos, 230, null);
		
		//Images des objets
		for (int i = 0; i < this.tabObjets.size(); i++) {
			g2.drawImage(this.tabObjets.get(i).getImgObjet(), this.tabObjets.get(i).getX(), this.tabObjets.get(i).getY(), null); }
		
		for (int i = 0; i < this.tabBillets.size(); i++) {
			g2.drawImage(this.tabBillets.get(i).bouge(), this.tabBillets.get(i).getX(), this.tabBillets.get(i).getY(), null); }
		
		
		g2.drawImage(this.imgPortail, 4700 - this.xPos, 170, null);
		g2.drawImage(this.imgGuichet, 4500 - this.xPos, 280, null);
		
		if(this.eRobot.isSaut()) {g2.drawImage(this.eRobot.saute(), this.eRobot.getX(), this.eRobot.getY(), null ); }
		else {g2.drawImage(this.eRobot.marche("Robot", 25), this.eRobot.getX(), this.eRobot.getY(), null ); }
		
		g2.setFont(police);
		g2.setColor(new Color(255, 255, 255));
		g2.drawString(this.score.getNbreBillets() + " parchemins trouv�s sur " + this.score.getNOMBRE_TOTAL_BILLETS(), 460, 25);
		
		
		g2.setColor(new Color(255, 255, 255));
		g2.drawString(this.temps.getStr(), 5, 25);
		
		if (finPartie() == false) {
			Font PoliceFinPartie = new Font("Arial", Font.BOLD, 20 );
			g2.setFont(PoliceFinPartie); }
		if(win() == true) {
			g2.drawString("Bravo, tu as r�ussi ce niveau !", 230, 150); }
	   	else if (lose() == true) {
	    	g2.drawString("Dommage, le temps est �coul� !", 230, 150) ; }
			
		
	}

}
