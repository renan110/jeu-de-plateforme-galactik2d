package fr.renan.jeu;


import javax.swing.JFrame;

import fr.renan.audio.Audio;


public class MainProgram {
	
	//FIELDS
	public static Scene scene;

	//METHODS
	public static void main(String[] args) {
		
		
		//Creation de la fen�tre de l'application
		JFrame fenetre = new JFrame("Galactik2D");
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fenetre.setSize(700, 360);
		fenetre.setLocationRelativeTo(null);
		fenetre.setResizable(false);
		fenetre.setAlwaysOnTop(true);
		
		//Instanciation de l'objet scene
		scene = new Scene();
		
		fenetre.setContentPane(scene); //On associe la scene � la fenetre
		fenetre.setVisible(true);
		
		Audio.playSound("/audio/Space.wav");
		
	}

}

