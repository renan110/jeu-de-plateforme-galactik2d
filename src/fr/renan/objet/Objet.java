package fr.renan.objet;

import java.awt.Image;

import javax.swing.ImageIcon;

import fr.renan.jeu.MainProgram;

public class Objet {
	
	//FIELDS
	private int largeur, hauteur;
	private int x, y;
	protected Image imgObjet;
	protected ImageIcon icoObjet;
	
	//CONSTRUCTOR
	public Objet(int largeur, int hauteur, int x, int y) {
		super();
		this.largeur = largeur;
		this.hauteur = hauteur;
		this.x = x;
		this.y = y;
	}

	//GETTERS
	public int getLargeur() {
		return largeur;
	}

	public int getHauteur() {
		return hauteur;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	
	public Image getImgObjet() {
		return imgObjet;
	}

	//SETTERS
	public void setLargeur(int largeur) {
		this.largeur = largeur;
	}

	public void setHauteur(int hauteur) {
		this.hauteur = hauteur;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}
	

	
	
	//METHODS
	public void deplacement() {
		
		if(MainProgram.scene.getxPos() >= 0) {
			this.x = this.x - MainProgram.scene.getDx();
		}
	}
	
	
	
	
	

}
