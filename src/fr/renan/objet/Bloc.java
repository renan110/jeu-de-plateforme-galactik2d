package fr.renan.objet;

import javax.swing.ImageIcon;

public class Bloc extends Objet {
	
	//FIELDS
	
		
		
		//CONSTRUCTOR
		public Bloc(int x, int y) {
			
			super(28, 28, x, y);
			super.icoObjet = new ImageIcon(getClass().getResource("/images/Bloc1.png"));
			super.imgObjet = this.icoObjet.getImage();
		}

		
		//GETTERS
		
		
		//SETTERS
		
		//METHODS

}
