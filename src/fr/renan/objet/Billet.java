package fr.renan.objet;

import java.awt.Image;

import javax.swing.ImageIcon;



public class Billet extends Objet implements Runnable {
	//FIELDS
	private int compteur;
	private final int PAUSE = 15; //Temps d attente entre 2 tours de boucle
	
	//CONSTRUCTOR
	public Billet(int x, int y) {
		super(25, 20, x, y);
		
		super.icoObjet = new ImageIcon(getClass().getResource("/images/Billet1.png"));
		super.imgObjet = super.icoObjet.getImage();
	}
	
	//GETTERS
	
	//SETTERS
	
	//METHODS
	public Image bouge() { //mouvement du billet
		String str;
		ImageIcon ico;
		Image img;
		
		this.compteur++;
		if(this.compteur / 50 == 0) { str = "/images/Billet1.png"; }
		else { str = "/images/Billet2.png"; }
		if (this.compteur == 100) {this.compteur = 0;}
		
		ico = new ImageIcon(getClass().getResource(str));
		img = ico.getImage();
		return img;
		
	}

	@Override
	public void run() {
		try {Thread.sleep(20);} //On attend 20ms avant d appeler bouge pour que tous les objets aient le temps d etre cr�es
		catch (InterruptedException e) {}
		while (true) {         //Boucle infinie
			this.bouge();
			try{Thread.sleep(PAUSE);}
			catch(InterruptedException e) {}
		}
	}

}
