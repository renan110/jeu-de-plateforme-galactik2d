package fr.renan.objet;

import javax.swing.ImageIcon;

public class Mur extends Objet{
	
	//FIELDS
	
	
	
	//CONSTRUCTOR
	public Mur(int x, int y) {
		
		super(35, 97, x, y);
		super.icoObjet = new ImageIcon(getClass().getResource("/images/Colonne.png"));
		super.imgObjet = this.icoObjet.getImage();
	}


	//GETTERS
	
	
	
	//SETTERS
	
	//METHODS

}
