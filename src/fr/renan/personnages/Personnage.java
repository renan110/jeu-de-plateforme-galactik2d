package fr.renan.personnages;

import java.awt.Image;

import javax.swing.ImageIcon;

import fr.renan.jeu.MainProgram;
import fr.renan.objet.Objet;

public class Personnage {
	//FIELDS (Variables)
	private int largeur, hauteur;  //Dimensions du personnage
	private int x, y;              //Position du personnage
	protected boolean marche;        //Vrai si le personnage marche
	protected boolean versDroite;    //Vrai si le personnage est tourn� vers la droite
	protected int compteur;          //Compteur des pas du personnage
	

	
	
	//CONSTRUCTOR
	public Personnage(int x, int y, int largeur, int hauteur) {
		super();
		this.x = x;
		this.y = y;
		this.largeur = largeur;
		this.hauteur = hauteur;
		this.marche = false;
		this.versDroite = true;
		this.compteur = 0;
	}

	
	//GETTERS
	public int getLargeur() { return largeur; }

	public int getHauteur() { return hauteur; }
	
	public int getX() { return x; }
	
	public int getY() { return y; }
	
	public int getCompteur() { return compteur; }
	
	public boolean isMarche() { return marche; }
	
	public boolean isVersDroite() { return versDroite; }

	
	
	//SETTERS

	public void setLargeur(int largeur) { this.largeur = largeur; } //PAS BESOIN

	public void setHauteur(int hauteur) { this.hauteur = hauteur; } //PAS BESOIN

	public void setX(int x) { this.x = x; }

	public void setY(int y) { this.y = y; }
	
	public void setCompteur(int compteur) { this.compteur = compteur; }
	
	public void setMarche(boolean marche) { this.marche = marche; }

	public void setVersDroite(boolean versDroite) { this.versDroite = versDroite; }
	
	
	//METHODS
	
	public Image marche(String nom, int frequence) {
		
		String str;
		ImageIcon ico;
		Image img;
		
		if(this.marche == false) {
			if(this.versDroite == true) { str = "/images/" + nom + "Droite.png"; } 
			else { str = "/images/" + nom + "Gauche.png"; } 
		  } else {
			this.compteur++;
			if (this.compteur / frequence == 0) {
				if(this.versDroite == true) { str = "/images/" + nom + "Droite.png"; }
				else { str = "/images/" + nom + "Gauche.png"; }
			} else {
				if(this.versDroite == true) { str = "/images/" + nom + "MarcheDroite.png"; }
		        else { str = "/images/" + nom + "MarcheGauche.png"; }
		  }
			if(this.compteur == 2 * frequence) { this.compteur = 0; }
		}
		
		//Affichage de l'image du personnage
		ico = new ImageIcon(getClass().getResource(str));
		img = ico.getImage();
		return img;
	}
	
	
	public void deplacement() {
		
		if(MainProgram.scene.getxPos() >= 0) {
			this.x = this.x - MainProgram.scene.getDx();} }
	
	//Detection contact � droite
	public boolean contactAvant(Objet objet) {
		if(this.x + this.largeur < objet.getX() || this.x + this.largeur > objet.getX() + 5 || 
				this.y + this.hauteur <= objet.getY() || this.y >= objet.getY() + objet.getHauteur()){
				return false;
		
		} else { return true; } 
		
	}
	
	//Detection du contact � gauche
	public boolean contactArriere(Objet objet) {
		if(this.x > objet.getX() + objet.getLargeur()|| this.x + this.largeur < objet.getX() + objet.getLargeur() - 5|| 
				this.y + this.hauteur <= objet.getY() || this.y >= objet.getY() + objet.getHauteur()){
				return false;
		
		} else { return true; } 
	}
	
	//Detection contact dessous
	public boolean contactDessous(Objet objet) {
		if(this.x + this.largeur < objet.getX() + 5 || this.x > objet.getX() + objet.getLargeur() - 5|| 
				this.y + this.hauteur < objet.getY() || this.y + this.hauteur > objet.getY() + 5){
				return false;
			
		} else { return true; } 
	}
		
	
	//Detection contact Dessus
	public boolean contactDessus(Objet objet) {
		if(this.x + this.largeur < objet.getX() + 5 || this.x > objet.getX() + objet.getLargeur() - 5 || 
				this.y < objet.getY() + objet.getHauteur() || this.y > objet.getY() + objet.getHauteur() + 5){ return false; } 
		else { return true;}
	}
		
		
	public boolean proche(Objet objet) {
		if((this.x > objet.getX() - 10 && this.x < objet.getX() + objet.getLargeur() + 10) 
		|| (this.x + this.largeur > objet.getX() - 10 && this.x + this.largeur < objet.getX() + objet.getLargeur() + 10)){return true; }
		else {return false;}
}

	
	
	
	

}
