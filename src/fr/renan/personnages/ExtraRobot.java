package fr.renan.personnages;

import java.awt.Image;

import javax.swing.ImageIcon;

import fr.renan.jeu.MainProgram;
import fr.renan.objet.Billet;
import fr.renan.objet.Objet;

public class ExtraRobot extends Personnage {

	//FIELD
	private Image imgRobot;
	private ImageIcon icoRobot;
	private boolean saut; 
	private int compteurSaut; //dur�e du saut
	private boolean tombe;
	

	
	//CONSTRUCTOR
	public ExtraRobot(int x, int y) {
		
		super(x, y, 40, 50);
		this.icoRobot = new ImageIcon(getClass().getResource("/images/RobotDroite.png"));
		this.imgRobot = this.icoRobot.getImage();
		
		this.saut = false;
		this.compteurSaut = 0;
		this.tombe = false;
		
	}
	
	//GETTERS
	public Image getImgRobot() { return imgRobot; }

	public boolean isSaut() {
		return saut;
	}

	
	public boolean isTombe() {
		return tombe;
	}

	

	//SETTERS
	public void setSaut(boolean saut) {
		this.saut = saut;
	}
	
	public void setTombe(boolean tombe) {
		this.tombe = tombe;
	}
	
	//METHODS   //

	
	public Image saute() {
		
		ImageIcon ico;
		Image img;
		String str;
		
		this.compteurSaut++;
		//Mont�e du saut
		if(this.compteurSaut <=45) {
			if(this.getY() > MainProgram.scene.getHauteurPlafond()) {
				this.setY(this.getY() - 3);
			} else { this.compteurSaut = 46; }
		if (this.isVersDroite() == true) {str = "/images/RobotSautDroite.png";}
		else {str = "/images/RobotSautGauche.png";}
		
		//Retomb�e du saut
	    } else if(this.getY() + this.getHauteur() < MainProgram.scene.getySol()) {this.setY(this.getY() + 1);
		if(this.isVersDroite() == true) {str = "/images/RobotSautDroite.png";}
		else {str = "/images/RobotSautGauche.png";}
		
		//Saut termin�
		} else {
			if(this.isVersDroite() == true) {str = "/images/RobotDroite.png";}
			else {str = "/images/RobotGauche.png";}
			this.saut = false;
			this.compteurSaut = 0;
		}
		
		//Affichage de l'image du robot
		ico = new ImageIcon(getClass().getResource(str));
		img = ico.getImage();
		return img;
	}
	
	public Image tomber () {
		ImageIcon ico;
		Image img;
		String str;
		//Retomb�e du saut
    if(this.getY() + this.getHauteur() < MainProgram.scene.getySol()) {this.setY(this.getY() + 1);
	if(this.isVersDroite() == true) {str = "/images/RobotSautDroite.png";}
	else {str = "/images/RobotSautGauche.png";}
	this.tombe = true;
	
	//Saut termin�
	} else {
		if(this.isVersDroite() == true) {str = "/images/RobotDroite.png";}
		else {str = "/images/RobotGauche.png";}
		this.tombe = false;
		
	}
	
	//Affichage de l'image du robot
	ico = new ImageIcon(getClass().getResource(str));
	img = ico.getImage();
	return img;
		}
	
		
	
	
	

	public void contactObjet(Objet objet) {
		
		//contact horizontal
		if((super.contactAvant(objet) == true && this.isVersDroite() == true) || 
				(super.contactArriere(objet) == true && this.isVersDroite() == false))
				{MainProgram.scene.setDx(0);
				this.setMarche(false);
				}
		
		
		//contact avec objet en dessous
		if(super.contactDessous(objet) == true && this.saut == true) { //Robot saute sur un objet
			MainProgram.scene.setySol(objet.getY());
		}else if (super.contactDessous(objet) == false) {              //Robot tombe sur le sol initial
			MainProgram.scene.setySol(320);                            //Altitude initiale du sol
			if(this.saut == false) {this.tomber();}                   //Altitude initiale du robot
	}
		
		
		//contact avec object au-dessus
		if(super.contactDessus(objet) == true) {
		MainProgram.scene.setHauteurPlafond(objet.getY() + objet.getHauteur()); //Le plafond devient le dessous de l'objet
		} else if (super.contactDessus(objet) == false && this.saut == false) {
			MainProgram.scene.setHauteurPlafond(0); }                           //Altitude du plafond initiale ciel
		}
	
	
	public boolean contactBillet(Billet billet) {
		if(this.contactArriere(billet) == true || this.contactAvant(billet) == true ||
		   this.contactDessous(billet) == true || this.contactDessus(billet) ==true) { return true; }
		else {return false; }
}
}
