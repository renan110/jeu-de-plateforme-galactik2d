package fr.renan.affichage;

public class Temps implements Runnable {
	//FIELDS
	private final int PAUSE = 1000;
	private int compteurTemps;
	private String str;
	
	//CONSTRUCTOR
	public Temps() {
		this.compteurTemps = 100;
		this.str = "Temps restant : " + this.compteurTemps;
		
		Thread compteurTemps = new Thread(this);
		compteurTemps.start();
		
	}
	
	
	//GETTERS

	public int getCompteurTemps() {
		return compteurTemps;
	}
	

	public String getStr() {
		return str;
	}
	
	//SETTERS


	//METHODS
	@Override
	public void run() {
		while(true) {
			try {Thread.sleep(PAUSE); }
		    catch (InterruptedException e) {}
			this.compteurTemps--;
			this.str = "Temps restant : " + this.compteurTemps;
		}
		
	}



}
