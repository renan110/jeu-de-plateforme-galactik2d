package fr.renan.affichage;

public class Score {
	//FIELDS
	private final int NOMBRE_TOTAL_BILLETS = 10;
	private int nbreBillets;
	
	//CONSTRUCTOR
	public Score() {
		this.nbreBillets = 0;
		
	}

	
	    //GETTERS
	    public int getNbreBillets() {
		return nbreBillets; }
		

		public int getNOMBRE_TOTAL_BILLETS() {
			return NOMBRE_TOTAL_BILLETS; }
	

		//SETTERS
	    public void setNbreBillets(int nbreBillets) {
		this.nbreBillets = nbreBillets;
	}

	

}
